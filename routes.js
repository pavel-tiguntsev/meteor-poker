Accounts.onLogin(function () {
    FlowRouter.go('tables')
})

FlowRouter.route('/', {
    name: 'home',
    title: "Welcome to LuxPoker",
    action() {
        GAnalytics.pageview();
        BlazeLayout.render('HomeLayout');
    }
});

FlowRouter.route('/tables/6/:name', {
    name: 'table6',
    action() {
        BlazeLayout.render("Table6sLayout");
    }

})

FlowRouter.route('/referal=:username', {
    name: 'byreferal',
    action() {
        BlazeLayout.render('HomeLayout');
    }

})

FlowRouter.route('/tables', {
    name: 'tables',
    action() {
        BlazeLayout.render('MainLayout', {main: "TablesList"});
    }
});

FlowRouter.route('/cashier', {
    name: 'cashier',
    action() {
        BlazeLayout.render('MainLayout', {main: "Cashier"});
    }
});
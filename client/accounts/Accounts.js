var myLogoutFunc = function() {
	Session.set('nav-toggle', '');
	FlowRouter.go('/');
}

var myLoginFunc = function() {
	Session.set('nav-toggle', '');
	if (!!this.userId) {FlowRouter.go('/tables')};
	
}

AccountsTemplates.configure({
	termsUrl: 'terms-of-use',
	privacyUrl: 'privacy',
	onLogoutHook: myLogoutFunc,
	onSubmitHook: myLoginFunc
});

AccountsTemplates.addFields([
	{
		_id: 'firstName',
		type: 'text',
		displayName: 'First Name',
		required: true,
    	re: /(?=.*[a-z])(?=.*[A-Z])/,
    	errStr: '1 lowercase and 1 uppercase letter reqiured'		
	},
	{
		_id: 'LastName',
		type: 'text',
		displayName: 'Last Name',
		required: true,
    	re: /(?=.*[a-z])(?=.*[A-Z])/,
    	errStr: '1 lowercase and 1 uppercase letter reqiured'		
	},
	{
		_id: 'nick',
		type: 'text',
		displayName: 'NickName',
		required: true,
    	re: /(?=.*[a-z])(?=.*[A-Z])/,
    	errStr: '1 lowercase and 1 uppercase letter reqiured'		
	},

	 {
		_id: 'gender',
		type: 'select',
		displayName: 'Sex',
		select: [
			{
				text:'Male',
				value: 'Male'
			}, {
				text: 'Female',
				value: 'Female'
			}
		]
	}

]);
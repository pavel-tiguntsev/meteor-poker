

Template.TablesList.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('tables');
    });
});

Template.TablesList.helpers({
    tables: () => {
        return Tables.find({});
    },
    admin: () => {
    	return Roles.userIsInRole(Meteor.userId(), 'admin')
    }
});

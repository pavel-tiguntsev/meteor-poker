Template.Sit.onCreated(function(){
    var self = this;
    self.autorun(function(){
        self.subscribe('cards');
         self.subscribe('sits');
    });
});

Template.Sit.helpers({
cards: () => {
        var tablename = FlowRouter.getParam('name');
        return Cards.findOne({SessionId: Tables.findOne({name: tablename}).SessionId}).cards;
    },
dealer: () => {
  return Tables.findOne({name: FlowRouter.getParam('name')}).DealerId;
}
});


Template.Sit.events({
  'submit #sitdown'(event) {
  	    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const money = target.money.value;
    // Insert a task into the collection
$("#adduser").hide('slow');
    Meteor.call('sits.insert', Meteor.userId(), money, FlowRouter.getParam('name'), function (error, result) { if(error) {$("#adduser").show('slow');$("#adduser").html('LOW BALANCE'); setTimeout( function () {$("#adduser p").html('')}, 5000)}
      else {$("#adduser").hide('slow')}
    }) 
      
  
    
    
    // Clear form
    target.money.value = 0;
  },
  
  'click .addbutton'(event) {
  	//if  Sits.find({table: tablename, lid: this.getid[:-1]})
	//Meteor.call('sits.update', 	this.getid);
  if (Sits.find({table: FlowRouter.getParam('name'), user: Meteor.userId()}).count()) {
      throw new Meteor.Error('You have been seating in this table');
    }
  		$("#adduser").show('slow');
  		Meteor.call('sits.betaken', this._id, Meteor.userId(), FlowRouter.getParam('name'));

  },

  'click #close'(event) {
  //Meteor.call('sits.update',  this.getid[:-1]);
    $("#adduser").hide('slow');
    Meteor.call('sits.notbetaken', Meteor.userId(), FlowRouter.getParam('name'));
  },

});

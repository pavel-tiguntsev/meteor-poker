Template.Table6sLayout.onCreated(function(){
    var self = this;
    self.autorun(function(){
        var name = FlowRouter.getParam('name');
        self.subscribe('tables');
        self.subscribe('cards');
        self.subscribe('sits', {fields: {cards: 0}});
    });
});

Template.Table6sLayout.helpers({
    tables: () => {
        var tablename = FlowRouter.getParam('name');
        return Tables.findOne({name: tablename});
    },

    sits: () => {
        var tablename = FlowRouter.getParam('name');
        return Sits.find({table: tablename}, {sort: {sitplace:1}})
    },

    playermove: () => {
        var tablename = FlowRouter.getParam('name');
        return (Meteor.userId() === Tables.findOne({name: tablename}).ActivePlayer)
    },
    active: () => {
        return Sits.findOne({user: Meteor.userId(), table: FlowRouter.getParam('name')}).isActive;
    }

});

Template.Table6sLayout.events({

  'click #standup'(event) {
  	 $(".add").hide('slow');
    Meteor.call('sits.notbetaken', Meteor.userId(), FlowRouter.getParam('name'));
  },
'click #makeActive'(event) {
    Meteor.call('makeActive', Meteor.userId(), FlowRouter.getParam('name'));
  },
  'click #restart'(event) {
    Meteor.call('startgame', FlowRouter.getParam('name'));
  },
    'submit #addmoneyform'(event) {
        // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    const count = target.count.value;
    // Insert a task into the collection
$(".add #addmoney").hide('slow');
    Meteor.call('addchips', Meteor.userId(), count, FlowRouter.getParam('name'), function (error, result) { if(error) {$(".add #addmoney").show('slow');$(".add #addmoney").html('LOW BALANCE'); setTimeout( function () {$(".add #addmoney p").html('')}, 5000)}
      else {$(".add #addmoney").hide('slow')}
    }) 
       
    
    // Clear form
    target.count.value = 0;
  },
  
  'click .addmoneybutton'(event) {
    $("#addmoney").show('slow');
  //if (Sits.find({table: FlowRouter.getParam('name'), user: Meteor.userId()}).count()) {
    //  throw new Meteor.Error('You have been seating in this table');
    //}
  },

  'click #closeaddmoney'(event) {
  //Meteor.call('sits.update',  this.getid[:-1]);
    $("#addmoney").hide('slow');
  },

});




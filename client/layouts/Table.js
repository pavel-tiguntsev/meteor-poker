Template.Table.helpers({
    updateTableId: function() {
    	return this._id
    },
    admin: () => {
        return Roles.userIsInRole(Meteor.userId(), 'admin')
    }
});

Template.Table.events({
    'click #removeTable': function() {
        Meteor.call("deleteTable", this._id);
    }
});

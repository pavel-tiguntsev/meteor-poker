Template.MovePanel.onCreated(function(){
    var self = this;
    self.autorun(function(){
         self.subscribe('sits');
    });
});

Template.MovePanel.helpers({
tocall: () => {
  return Tables.findOne({name: FlowRouter.getParam('name')}).betround  - Sits.findOne({user: Meteor.userId()}).bet
},
betround: () => {
  return Tables.findOne({name: FlowRouter.getParam('name')}).betround
},
blind: () => {
  return Tables.findOne({name: FlowRouter.getParam('name')}).bBlind
},
PlayerMoney: () => {
  return Sits.findOne({user: Meteor.userId()}).money
},
min: () => {
  return Math.min(Sits.findOne({user: Meteor.userId()}).money, Tables.findOne({name: FlowRouter.getParam('name')}).betround);
},
allInOnly: () => {
  return (Tables.findOne({name: FlowRouter.getParam('name')}).betround  - Sits.findOne({user: Meteor.userId()}).bet) > Sits.findOne({user: Meteor.userId()}).money
}
})

Template.MovePanel.events({
  

      //    $('#check').click( function () { Meteor.call('bet', 0, Meteor.userId(), FlowRouter.getParam('name'));
    //    $('#bet').click( function () { Meteor.call('bet',, Meteor.userId(), FlowRouter.getParam('name'))});
     //   $('#call').click( function () { Meteor.call('bet', {{tocall}}, Meteor.userId(), FlowRouter.getParam('name'))});


'click #fold'(event) {
  event.preventDefault();
  Meteor.call('fold', Sits.findOne({user: Meteor.userId()})._id, Sits.findOne({user: Meteor.userId()}).sitplace, FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
},
'click #check'(event) {
  event.preventDefault();
  Meteor.call('bet', Tables.findOne({name: FlowRouter.getParam('name')}).betround  - Sits.findOne({user: Meteor.userId()}).bet, Meteor.userId(), FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
},
'click #call'(event) {
  event.preventDefault();
  Meteor.call('bet', Tables.findOne({name: FlowRouter.getParam('name')}).betround  - Sits.findOne({user: Meteor.userId()}).bet, Meteor.userId(), FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
},
'click #bet'(event) {
  event.preventDefault();
  Meteor.call('bet', parseInt($("#betnum").val()), Meteor.userId(), FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
},
'click #allin'(event) {
  event.preventDefault();
  Meteor.call('bet', Sits.findOne({user: Meteor.userId()}).money, Meteor.userId(), FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
}


});

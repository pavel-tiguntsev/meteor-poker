Template.Chat.events({
  'submit #chatinput'(event) {
    event.preventDefault();
    const target = event.target;
    const text = target.text.value;
    Meteor.call('chat.insert', text, FlowRouter.getParam('name'), function (error, result) { if(error) {}
    else {}})
    target.text.value = '';
    }

  });

Template.Chat.onCreated(function(){
  var self = this;
  self.autorun(function(){
    var name = FlowRouter.getParam('name');
    self.subscribe('chats');
  });
});

Template.Chat.helpers({
      chats: () => {
      var name = FlowRouter.getParam('name');
      return Chats.find({table: name}, {sort: {createdAt: -1}})
}
});

  
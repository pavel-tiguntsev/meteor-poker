Template.Cashier.events({
  'submit #depositeplaymoney'(event) {
  	    // Prevent default browser form submit
    event.preventDefault();

    // Get value from form element
    const target = event.target;
    var money = parseInt(target.money.value);
    console.log(typeof(money))
    Meteor.call('addplaymoney', Meteor.userId(), money);
    // Clear form
    target.money.value = 0;
  }
})
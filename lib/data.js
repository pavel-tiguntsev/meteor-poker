Tables = new Mongo.Collection('tables');
Sits = new Mongo.Collection('sits');
Cards = new Mongo.Collection('cards');
Chats = new Mongo.Collection('chats');
LogSession = new Mongo.Collection('session');
MoveLog = new Mongo.Collection('movelog');

// Generator function. Returns a function which returns incrementing
// Fibonacci numbers with each call.

Tables.allow({
    insert: function(userId, doc) {
        return !!Roles.userIsInRole(userId, 'admin');
    }
})
MoveLog.allow({
    insert: function(userId, doc) {return !!this.userId}})

TablesSchema = new SimpleSchema({
    name: {
        type: String,
        label: "Table Name",
    },
    SessionId: {
        type: String,
        label: "SessionId"
    },
    ActivePlayer: {
        type: String
    },
    nos: {
        type: Number,
        label: "Number Of Sits"
    },
    nop: {
        type: Number,
        label: "Number Of Players",
        defaultValue: 0,
    },
    sBlind: {
        type: Number,
        label: "Small Blind"
    },
    bBlind: {
        type: Number,
        label: "Big Blind"
    },
    MinBuyIn: {
        type: Number,
        label: "Min Buy-in"
    },
    MaxBuyIn: {
        type: Number,
        label: "Max Buy-in"
    },
    DealerId: {
        type: Number,
        label: "DealerId",
        defaultValue: 0
    },
    bank: {
        type: Number
    },
    round: {
        type: Number
    },
    betround: {
        type: Number
    },
    isInAction: {
        type: Boolean
    },
    sitplaces: {
        type: [Number]
    },
    tablecards: {
        type: [String]
    }
});

Tables.attachSchema(TablesSchema);

if (Meteor.isServer) {
  // This code only runs on the server
  // Only publish tasks that are public or belong to the current user

}


Meteor.methods({

});

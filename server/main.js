import { Meteor } from 'meteor/meteor';

Meteor.startup(() => {
	console.log("Hello!!!");
});



Meteor.methods({

    deleteTable: function(tableId) {
        Tables.remove(tableId);
    },
    'makeActive'(user, table) {
        Sits.update({user: user, table: table}, {$set: {isActive: true}});
        Meteor.call('toStart', table);
   },
    'addplaymoney'(user, money) {
            Meteor.users.update(user, { $inc: {balance: money}})

    },

    'chat.insert'(text, table) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    Chats.insert({
      table,
      text,
      createdAt: new Date(),
      username: Meteor.users.findOne(this.userId).profile.nick,
    });
  },
  'addchips'(user, count, table){
    var sitbalance = Sits.findOne({user: user, table: table}).money;
    balance = parseInt(Meteor.user().balance);
    count = parseInt(count);
    max = Tables.findOne({name: table}).MaxBuyIn - sitbalance;
    
    //Добавить ошибку если не парсится
    if (Meteor.user().balance < count) {
        throw new Meteor.Error('LowBalance');
        return false;
    }

    if ( count > Tables.findOne({name: table}).MaxBuyIn) {
        throw new Meteor.Error('over-max-buyin', );
        return false;
    }

    Meteor.users.update(user, { $ins: {balance: - count}})
    
    Sits.update({user: user, table: table} , { $inc: {money: count}});
    Meteor.call('toStart', table);
  },
  'sits.insert'(user, buyin, table) {
    balance = parseInt(Meteor.user().balance);
    // Make sure the user is logged in before inserting a task
    buyin = parseInt(buyin);
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if (Meteor.user().balance < buyin) {
        throw new Meteor.Error('LowBalance');
        return false;
    }
    if ( buyin < Tables.findOne({name: table}).MinBuyIn) {
        throw new Meteor.Error('Minimum-buyin-is-required');
        return false;
    }
    if ( buyin > Tables.findOne({name: table}).MaxBuyIn) {
        throw new Meteor.Error('over-max-buyin', );
        return false;
    }
        Meteor.users.update(user, { $inc: {balance: - buyin}})
    
    Sits.update({user: user, table: table} , { $set: {
        user: user,
        money: buyin,
        isActive: true,
        }
    });

    Meteor.call('toStart', table);
    return true;
},
    'startgame'(table) {
    
    
	    LogSession.insert({table, tablecards: LogSession.findOne("pXBgRhAefBQfqfXsp").start, date: new Date()});
	    SessionId = LogSession.findOne({table:table}, {sort: {date: -1}})._id;
	    Sits.update({table: table, money: 0}, {$set: {isActive: false}}, { multi: true });
	    Sits.update({table: table}, {$set: {inplay: false, bet: 0, gamebet: 0}}, { multi: true });
	    Tables.update({name: table}, {$set: {SessionId: SessionId, isInAction: true, round: 0, bank: 0, betround: 0, tablecards: []}});
	    var NoS =  Tables.findOne({name: table}).nos
	    var sitplaces = [];
	    for (var i = 1; i <= NoS; i++) {
	        if (Sits.find({table: table, isActive: true, sitplace: i}).count()) {
	            console.log(i)
	            sitplaces.push(i);

	        }
	    }

	    console.log(sitplaces)
	    Tables.update({name: table}, {$set: {sitplaces: sitplaces}});
	    for(var i = 0; i < sitplaces.length; i++) {
	        Cards.insert({SessionId: SessionId, Owner: Sits.findOne({sitplace: sitplaces[i]}).user, cards: Meteor.call('getCards', 2, SessionId)});
	        Sits.update({sitplace: sitplaces[i]}, {$set: {inplay: true, moved: false, allin: false, bet: 0}})
	    }
	    var DealerId = Tables.findOne({name: table}).DealerId + 1;
	     
	    var isDealer = false;

	    for(var i = DealerId; i <= NoS; i++ ){
	        if (sitplaces.indexOf(i) + 1) {
	            DealerId = i;
	            Tables.update({name: table}, {$set: {DealerId: i}});
	            isDealer = true;
	            break;
	        }
	    }
	    if (!isDealer) {
	        for(var i = 1; i < DealerId; i++){
	            if (sitplaces.indexOf(i) + 1) {
	            DealerId = i;
	            Tables.update({name: table}, {$set: {DealerId: i}});
	        }
	        }
	    }
	    

	     sBlindID = Meteor.call('nextPlayer', DealerId, sitplaces);
	     bBlindID = Meteor.call('nextPlayer', sBlindID, sitplaces);

	     console.log('Dealer:', DealerId);
	     console.log('Small Blind:', sBlindID);
	     console.log('Big Blind:', bBlindID);

	    var tableid = Tables.findOne({name: table})._id;
	    var sBlind = Tables.findOne({name: table}).sBlind;
	    var bBlind = Tables.findOne({name: table}).bBlind;

	    Tables.update(tableid, {$set: {bank:0}});
	    Tables.update(tableid, { $set: {betround: Tables.findOne(tableid).bBlind}});
	    Sits.update({table: table, sitplace: sBlindID}, { $set: {bet: sBlind, money: Sits.findOne({table: table, sitplace: sBlindID}).money - Tables.findOne({name: table}).sBlind}});
	    Sits.update({table: table, sitplace: bBlindID}, { $set: {bet: bBlind, money: Sits.findOne({table: table, sitplace: bBlindID}).money - Tables.findOne({name: table}).bBlind}});
	    Tables.update({name: table}, {$inc: {bank: sBlind + bBlind}});

	    Meteor.call('callmove', bBlindID, table)

},



'bet'(bet, user, table) {
	
	var sit = Sits.findOne({table: table, user: user});
    var sitid = sit._id;

    MoveLog.update(Sits.findOne(sit._id).moveSession, {$set: {bet, user, moved: true}});

    console.log(user, Tables.findOne({name: table}).ActivePlayer)
    if (user != Tables.findOne({name: table}).ActivePlayer) {
      throw new Meteor.Error('not-authorized');
    }
    if ((bet > sit.money) || (bet < 0) || (sit.money < 0)) {
      throw new Meteor.Error('low-balance');
    }
    var Tablemaxbet = Tables.findOne({name: table}).betround;
    var cp = sit.sitplace;
    var Totalbet = sit.bet + bet;
    console.log(Tablemaxbet, bet)

    if (Totalbet > Tablemaxbet) {
    	if (sit.money == bet) {
    		Sits.update(sitid, { $set: {allin:true}});
    		//if (Tables.findOne({name: table}).sitplaces.length - Sits.find({table:table, allin:true}).count() == 1) {Meteor.call('getresults', table)} 
    	}
        Sits.update(sitid, {$inc:{bet: bet}})
        Tables.update({name: table}, { $set: {betround: Totalbet}})
        Sits.update(sitid, { $set: {bet: Totalbet, money: sit.money - bet, moved: true}});
        Meteor.call('callmove', cp, table);
        Tables.update({name: table}, {$inc: {bank: bet}});
    } else {
    if (Totalbet == Tablemaxbet) {
    	if (sit.money == bet) {Sits.update(sitid, { $set: {allin:true}}); } 
        Tables.update({name: table}, {$inc: {bank: bet}});
        Sits.update(sitid, { $set: {bet: Totalbet, money: sit.money - bet, moved: true}});
        Meteor.call('callmove', cp, table)
    } else {
        if (sit.money == bet) {
            Tables.update({name: table}, {$inc: {bank: bet}});
            Sits.update(sitid, { $set: {bet: Totalbet, money: 0, moved: true, allin:true}});
        Meteor.call('callmove', cp, table)
        } else {
            Meteor.call('fold', sitid, cp, table);
        }
    
    }   
}

},
'callmove'(last, table){
	
	
		
    	var cTable = Tables.findOne({name: table});
    	var sitplaces = cTable.sitplaces;
    	var cp = Meteor.call('nextPlayer', last, sitplaces);
    	console.log('CurrentPlayer', cp)
    	var Player = Sits.findOne({table: table, sitplace: cp});
    	console.log('Player', Player)
    	if ((Player.moved && (Player.bet ==  cTable.betround)) || (sitplaces.length - Sits.find({table: table, allin: true}).count())) { 
            	console.log("nextround")
            	if (cTable.round < 3) {
                	Meteor.call('newround', table, sitplaces)
            	} else {
                Meteor.call('getresults', table)
            	}
        } else {
    	if ((!Player.isActive) || (Player.allin) ){
    	Sits.update(Player.id, { $set: {moved: true}});
        Meteor.call('callmove', cp, table);
    	} else {

    		
     	if (Player.inplay) {
     		var dateMove = new Date();
     		Tables.update({name:table}, {$set: { ActivePlayer: Player.user}});
     		MoveLog.insert({table, Session: Tables.findOne({name: table}).SessionId, round: Tables.findOne({name: table}).round, time: dateMove, moved: false});
     		Sits.update(Player._id, {$set: {moveSession: MoveLog.findOne({time: dateMove})._id }});
     		var Fiber = require('fibers');
     		Fiber( function () {
     			var fiber = Fiber.current;
            
            setTimeout(function() {fiber.run();
			}, 30000);
			console.log('ok')

        	Fiber.yield();
        	console.log('ko')
			if (!MoveLog.findOne(Sits.findOne(Player._id).moveSession).moved) {
        	Meteor.call('bet', 0, Player.user, table); 
        	console.log('Players left', sitplaces);
        	Sits.update(Player._id, {$set: {isActive: false}})
        	Meteor.call('callmove', cp, table);

    	}


    }).run();  }
        	



    		}}

    
 

        
},
'newround'(table, sitplaces) {
    console.log('newround')
    var DealerId = Tables.findOne({name: table}).DealerId;
    Tables.update({name: table}, { $inc: {round: 1}});

    if (Tables.findOne({name: table}).round == 1) {
        for(var i = 0; i < sitplaces.length; i++) {
            if (Sits.findOne({table: table, sitplace: sitplaces[i]}) != undefined) {
                console.log("debug")
        Sits.update({table: table, sitplace: sitplaces[i]}, {$set: { gamebet: Sits.findOne({table: table, sitplace: sitplaces[i]}).bet}});
        Sits.update({table: table, sitplace: sitplaces[i]}, {$set: {bet: 0}})
        if (!Sits.findOne({table: table, sitplace: sitplaces[i]}).allin) {Sits.update({table: table, sitplace: sitplaces[i]}, {$set: {moved: false}})} 
        }
    }
        
        Tables.update({name: table}, {$set: { betround: 0, tablecards:  Meteor.call('getCards', 3, Tables.findOne({name: table}).SessionId) }});
        Meteor.call('callmove', DealerId, table)
    } else {

    if (Tables.findOne({name: table}).round == 2) {
                for(var i = 0;i < sitplaces.length; i++) {
                    if (Sits.findOne({table: table, sitplace: sitplaces[i]}) != undefined) {
            Sits.update({table: table, sitplace: sitplaces[i]}, {$inc: { gamebet: Sits.findOne({table: table, sitplace: sitplaces[i]}).bet}})
            Sits.update({table: table,sitplace: sitplaces[i]}, {$set: {bet: 0}});
            if (!Sits.findOne({table: table, sitplace: sitplaces[i]}).allin) {Sits.update({table: table, sitplace: sitplaces[i]}, {$set: {moved: false}})}
        }}
    Tables.update({name: table}, {$addToSet: {tablecards:  Meteor.call('getCards', 1, Tables.findOne({name: table}).SessionId)[0] }, $set: { betround: 0}})
    Meteor.call('callmove', DealerId, table);
} else {
    if (Tables.findOne({name: table}).round == 3) {
                        for(var i = 0; i < sitplaces.length; i++) {
                             if (Sits.findOne({table: table, sitplace: sitplaces[i]}) != undefined) {
            Sits.update({table: table, sitplace: sitplaces[i]}, {$inc: { gamebet: Sits.findOne({table: table, sitplace: sitplaces[i]}).bet}})
        Sits.update({table: table,sitplace: sitplaces[i]}, {$set: {bet: 0}});
        if (!Sits.findOne({table: table, sitplace: sitplaces[i]}).allin) {Sits.update({table: table, sitplace: sitplaces[i]}, {$set: {moved: false}})}
        }}
        Tables.update({name: table}, {$addToSet: {tablecards:  Meteor.call('getCards', 1, Tables.findOne({name: table}).SessionId)[0] }, $set: { betround: 0}})
    Meteor.call('callmove', DealerId, table);
}}}
},
'fold'(sitid, cp, table){
    Sits.update(sitid, { $set: {inplay: false}});
    
    if (Sits.find({table: table, inplay: true}).count() < 2) {
        Meteor.call('passbank', Sits.findOne({table: table, inplay: true}).user, table);
        Meteor.call('endgame', table)
    } else {
        Tables.update({name: table}, {$pull: {sitplaces: cp}});
        Sits.update(sitid, {$set: {inplay:false}})
        Meteor.call('callmove', cp, table)
    }
    
},
'passbank'(user, table) {

    console.log('winner', Meteor.users.findOne(user).profile.nick);
    Sits.update({user: user, table: table}, { $inc: {money: Tables.findOne({name: table}).bank }});
    Tables.update({name: table}, {$set: {bank: 0}});
},
'endgame'(table){
	var NoS = Tables.findOne({name: table}).nos;
    console.log(NoS, table)
    for(var i = 1; i < NoS + 1; i++) {
    	console.log(i)
    	if (Sits.findOne({table: table, sitplace: i}).standIpNextRound) {
    		console.log(Sits.findOne({table: table, sitplace: i}).nick)
    		Meteor.call('standup', Sits.findOne({table: table, sitplace: i})._id);
    	}}
    console.log('done cleaning')
    Tables.update({name: table}, {$set: {ActivePlayer: "none", isInAction: false, round: 0, bank: 0, betround: 0, tablecards: []}});
    Meteor.call('toStart', table);
},
'toStart'(table) {
        if (!Tables.findOne({name: table}).isInAction) {
        if (Sits.find({table: table, isActive: true}).count() > 1) {
            Meteor.call('startgame', table);
        }
    }
},

'nextPlayer'(sitid, sitarray) {
    if (sitarray.indexOf(sitid) + 1 == sitarray.length){
        return sitarray[0]
    }
    return sitarray[sitarray.indexOf(sitid) + 1];
},

'sits.betaken'(id, user, table) {

    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if (Sits.find({table: table, user: user}).count()) {
      throw new Meteor.Error('You have been seating in this table');
    }
    if (!Sits.findOne(id, {fields: {isActive: 1}}).isTaken) {
        Sits.update(id, { $set: {
        user: user,
        nick: Meteor.user().profile.nick,
        isActive: false,
        isTaken: true}
        })
    }
    Meteor.call('cleanUnactive', id)
},
'cleanUnactive'(id){ 
    /*setTimeout( function(){
        if (!Sits.findOne(id, {fields: {isActive: 1}}).isActive) {
            Sits.update(id, { $set: {
                user: '',
                money: null,
                isTaken: false,
                nick: ''
            }
        })
    }}, 60000)*/},
'standup'(id) {
	console.log('standup', id, Sits.findOne(id).money, Sits.findOne(id).user)
	var money = Sits.findOne(id).money;
	Sits.update(id, { $set: {user: '',money: 0,isActive: false,isTaken: false,inplay: false,gamebet: 0,bet: 0,nick: '',standIpNextRound: false}});
	console.log('standed')
    Meteor.users.update(Sits.findOne(id).user, { $inc: {balance: money}})       
    },
'sits.notbetaken'(user, table) {
    // Make sure the user is logged in before inserting a task
    if (! this.userId) {
      throw new Meteor.Error('not-authorized');
    }

    if ((!Tables.findOne({name: table}).isInAction) || (!Sits.findOne({table: table, user: user}).inplay)) {
        Meteor.call('standup', Sits.findOne({table:table, user:user})._id)
    } else {
        Sits.update({table: table, user: user}, { $set: {
            standIpNextRound: true}
        });
    }

    //IMPROVE. 1 USER CAN TAKE ONLY 1 SIT ON EACH TABLE


},
	'getCards'(amount, SessionId) {
    var CardList = LogSession.findOne(SessionId).tablecards;
    var cards = [];
    for (var cd = 1; cd <= amount; cd++) {
        randomCardNum = Math.floor(Math.random() * CardList.length);
        card = CardList.splice(randomCardNum, 1)[0];
        cards.push(card)
    }
    console.log(CardList.length);
    LogSession.update(SessionId, {$set: {tablecards: CardList}});
    return cards;
},
'getresults'(table) {
    var Table = Tables.findOne({name: table});
    var TableBank = Table.bank;
    console.log(Table.sitplaces);
    var Tablecards = retype(Table.tablecards);
    console.log(Table.tablecards);
    var PlayersCards = {};
    var tempranks = [];
    var Bank = {};




for( var i = 0; i < Table.sitplaces.length; i++){
	var Owner = Sits.findOne({sitplace: Table.sitplaces[i]}).user
	Bank[Owner] = 0;
		Sits.update({table: table, sitplace: Table.sitplaces[i]}, {$inc: { gamebet: Sits.findOne({table: table, sitplace: Table.sitplaces[i]}).bet}});
		for(var j = 0; j < Table.sitplaces.length; j++) {
				Bank[Owner] += Math.min(Sits.findOne({sitplace: Table.sitplaces[j]}).gamebet, Sits.findOne({sitplace: Table.sitplaces[i]}).gamebet)
		}
		PlayersCards[Owner] = retype(Cards.findOne({SessionId: Table.SessionId, Owner:  Owner}).cards).concat(Tablecards).sort(Comparator);
		console.log(PlayersCards);
		console.log(Bank);
}

function retype(strings) {
    for(var i = 0; i < strings.length; i++) {
        strings[i] = [parseInt(strings[i].slice(0, strings[i].length - 1)), strings[i].slice(-1)]
    }
    return strings;
};

    var values = function (cards) {
    this.values = [];
    for (i = 0; i < cards.length; i++) {
        this.values.push(cards[i][0])
    }
    return this.values
};


var suits = function (cards) {
    lH = [];
    lC = [];
    lS = [];
    lD = [];
    for (i = 0; i < cards.length; i++) {
        if (cards[i][1] == 'H') {
            lH.push(cards[i])
        }
         if (cards[i][1] == 'C') {
         lC.push(cards[i])
         }
         if (cards[i][1] == 'S') {
         lS.push(cards[i])
         }
         if (cards[i][1] == 'D') {
         lD.push(cards[i])
         }

    }
    suitsarr = [lC, lD, lH, lS];
    return suitsarr;
};

function Comparator(a,b){
    if (a[0] < b[0]) return -1;
    if (a[0] > b[0]) return 1;
    return 0;
}

var isStraight = function (cards) {
    cards = Array.from(new Set(values(cards)));
    if (cards.length < 5) {
        return 0
    } else {
        next = cards[2];
        for (i = 2; i < 7; i++) {
            if (cards[i] == next) {
                next++
            } else {
                break
            }
            if (i == 6) {
                return [4].concat(cards[i])
            }
        }

        next = cards[1];
        for (i = 1; i < 6; i++) {
            if (cards[i] == next) {
                next++
            } else {
                break
            }
            if (i == 5) {
                return [4].concat(cards[i])
            }
        }
        next = cards[0];
        for (i = 0; i < 5; i++) {
            if (cards[i] == next) {
                next++
            } else {
                break
            }
            if (i == 4) {
                return [4].concat(cards[i])
            }

        }

        if (cards[cards.length - 1] == 14) {
            next = 2;
            for (i = 0; i < 4; i ++){
                if (cards[i] == next) {
                    next++;
                } else {
                    break
                }
                if (i == 3) {
                    return [4].concat([5])
                }
            }
        }


    } return 0;
};


var isStraightFlush = function (cards) {
    suitedcards = suits(cards);
    for (suit = 0; suit < 4; suit++){
        if (suitedcards[suit].length >= 5) {
            if (isStraight(suitedcards[suit])) {

                return [8].concat(isStraight(suitedcards[suit]))
            } else {
                return [5].concat(values(suitedcards[suit]).reverse())
            }
        }
    }

};
var ckind = function (cards) {
    this.countdict = {};
    cards = values(cards);
    for (i = 0; i < cards.length; i++){
        this.countdict[cards[i]] = 1;
    };
    for (i = 1; i < cards.length + 1; i++){
        if (cards[i] == cards[i - 1]) {
            this.countdict[cards[i]]++
        }
    }
    this.countdict = Object.keys(this.countdict).map(function(key) {
        return [this.countdict[key], parseInt(key)];
    });
    return this.countdict.sort(function(a, b){return (b[0]-a[0])||(b[1]-a[1])})
};
var isFour = function (cards) {
    if (ckind(cards)[0][0] == 4) {
        return [7].concat([ckind(cards)[0][1], ckind(cards)[1][1]])
    }
};

var isFull = function (cards) {
    if (ckind(cards)[0][0] == 3 && ckind(cards)[1][0] == 2) {
        return [6].concat([ckind(cards)[0][1], ckind(cards)[1][1]])
    }
};

var isThree = function (cards) {
    if (ckind(cards)[0][0] == 3) {
        return [3].concat([ckind(cards)[0][1], ckind(cards)[1][1], ckind(cards)[2][1]])
    }
};


var isTwoPairs = function (cards) {
    if (ckind(cards)[0][0] == 2 && ckind(cards)[1][0] == 2) {
        return [2].concat([ckind(cards)[0][1], ckind(cards)[1][1], ckind(cards)[2][1]])
    }
};

var isPair = function (cards) {
    if (ckind(cards)[0][0] == 2) {
        return [1].concat([ckind(cards)[0][1], ckind(cards)[1][1], ckind(cards)[2][1], ckind(cards)[3][1]])
    }
};

function getRank(cards) {
    if (isStraightFlush(cards)) return isStraightFlush(cards);
    if (isFour(cards)) return isFour(cards);
    if (isFull(cards)) return isFull(cards);
    if (isStraight(cards)) return isStraight(cards);
    if (isThree(cards)) return isThree(cards);
    if (isTwoPairs(cards)) return isTwoPairs(cards);
    if (isPair(cards)) return isPair(cards);
    return [0].concat(values(cards).slice(2,7).reverse())
}


for (i in PlayersCards) {
    tempranks.push([i, getRank(PlayersCards[i])])
}


tempranks.sort(function(a, b){return (b[1][0]-a[1][0])||(b[1][1]-a[1][1])||(b[1][2]-a[1][2])||(b[1][3]-a[1][3])||(b[1][4]-a[1][4])||(b[1][5]-a[1][5])})


console.log("Победители", tempranks)

var GroupWinners = [[tempranks[0].concat(Bank[tempranks[0][0]])]];
for (var w = 1; w < tempranks.length; w++ ) {
    if (tempranks[w][1].toString() == tempranks[w - 1][1].toString()) {
        GroupWinners[GroupWinners.length - 1].push(tempranks[w].concat(Bank[tempranks[w][0]]))
    } else {
        GroupWinners.push([tempranks[w].concat(Bank[tempranks[w][0]])])
    }
}
console.log("Groups", GroupWinners);
var PlayerPrize = {}
for(var i = 0; i < GroupWinners.length; i++){
	if (GroupWinners[i].length > 1) {
		GroupWinners[i].sort(function (a,b){return a[2] - b[2] })
		for(var j = 0; GroupWinners[i].length; j++){
			if (j == GroupWinners[i].length - 1) {
				PlayerPrize[GroupWinners[i][j][0]] = Math.min(Bank[GroupWinners[i][j][0]]), TableBank
				TableBank -= PlayerPrize[GroupWinners[i][j][0]];
				if (TableBank <= 0) {break}
			} else {
			PlayerPrize[GroupWinners[i][j][0]] = GroupWinners[i][j][2]/GroupWinners[i].length;
			TableBank -= GroupWinners[i][j][2]/GroupWinners[i].length;
			if (TableBank <= 0) {break} }
		}
	} else {
		PlayerPrize[GroupWinners[i][0][0]] = Math.min(GroupWinners[i][0][2], TableBank);
		TableBank -= PlayerPrize[GroupWinners[i][0][0]]
		if (TableBank <= 0) {break}
	}
}

console.log(PlayerPrize)
LogSession.update({SessionId: Tables.findOne({name: table}).SessionId}, {$set: {Winners: PlayerPrize}})

for(i in PlayerPrize) {
	Sits.update({user: i, table: table}, {$inc:{money: PlayerPrize[i]}});
}

Meteor.call('endgame', table, function (error, result) {});
}

})